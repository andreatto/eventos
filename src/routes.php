<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
// methods:
// GET: /participantes   |  Retrieve all todos
// GET: /participantes/search/bug   | Search for todos with ‘bug’ in their name
// GET: /participantes/1     | Retrieve todo with id == 1
// POST: /participantes         | 	Add a new todo
// PUT: /participantes/1        | Update todo with id == 1
// DELETE: /participantes/1      | Delete todo with id == 1

/*
$app->get('/', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

// Retrieve todo with id 
$app->get('/participante/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM tb_participantes WHERE part_id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $todos = $sth->fetchObject();
    return $this->response->withJson($todos);
});

// Search for todo with given search teram in their name
$app->get('/participante/nome/[{query}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM tb_participantes WHERE UPPER(part_nome) LIKE :query ORDER BY part_nome ");
    $query = "%" . $args['query'] . "%";
    $sth->bindParam("query", $query);
    $sth->execute();
    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);
});

// Retrieve On participantes
$app->get('/participantes/listOn', function (Request $request, Response $response, array $args) {

    $sth = $this->db->prepare("SELECT * FROM tb_participantes WHERE part_status = 0");
    $sth->execute();
    $todos = $sth->fetchAll();

    return $this->response->withJson($todos);
});

// Retrieve Off participantes
$app->get('/participantes/listOff', function (Request $request, Response $response, array $args) {

    $sth = $this->db->prepare("SELECT * FROM tb_participantes WHERE part_status = 1");
    $sth->execute();
    $todos = $sth->fetchAll();

    return $this->response->withJson($todos);
});

// Retrieve all participantes
$app->get('/participantes/listall', function (Request $request, Response $response, array $args) {

    $sth = $this->db->prepare("SELECT * FROM tb_participantes ORDER BY part_id");
    $sth->execute();
    $todos = $sth->fetchAll();

    return $this->response->withJson($todos);
});

// Verifica codigo de barras
$app->get('/scanner/[{query}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM tb_participantes WHERE UPPER(part_cod_barras) LIKE :query ");
    //$query = "%" . $args['query'] . "%";
    $query = $args['query'];
    $sth->bindParam("query", $query);
    $sth->execute();
    $todos = $sth->fetchObject();
    return $this->response->withJson($todos);
});

// Add a new todo
$app->post('/participante/add', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO tb_participantes (part_nome, part_cod_barras, part_qtd_acompanhantes, part_date, part_foto, part_status) VALUES (:nome, :cod_barras, :acompanhantes, :mData, :mFoto, :newStatus)";
    $sth = $this->db->prepare($sql);
    //$sth->bindParam("nome", $input['part_nome']);
    $sth->bindParam("nome", $input['part_nome']);
    $sth->bindParam("cod_barras", $input['part_cod_barras']);
    $sth->bindParam("acompanhantes", $input['part_qtd_acompanhantes']);
    $sth->bindParam("mData", $input['part_date']);
    $sth->bindParam("mFoto", $input['part_foto']);
    $sth->bindParam("newStatus", $input['part_status']);
    $sth->execute();
    $input['part_id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

// Update todo with given id
$app->put('/participante/[{part_id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE tb_participantes SET part_nome = :nome, 
                                        part_cod_barras = :cod_barras,
                                        part_qtd_acompanhantes = :acompanhantes,
                                        part_date = :mData,
                                        part_foto = :mFoto,
                                        part_status = :newStatus
                                    WHERE part_id = :id  ";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $args['part_id']);
    $sth->bindParam("nome", $input['part_nome']);
    $sth->bindParam("cod_barras", $input['part_cod_barras']);
    $sth->bindParam("acompanhantes", $input['part_qtd_acompanhantes']);
    $sth->bindParam("mData", $input['part_date']);
    $sth->bindParam("mFoto", $input['part_foto']);
    $sth->bindParam("newStatus", $input['part_status']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);

});

// DELETE a todo with given id
$app->delete('/participante/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("DELETE FROM tb_participantes WHERE part_id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);
});

*/

// REDE SOCIAL
// Add a new todo
$app->post('/usuario/add', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO tb_usuario (usu_nome, usu_email, usu_senha) VALUES (:nome, :email, :senha)";
    $sth = $this->db->prepare($sql);
    //$sth->bindParam("nome", $input['part_nome']);
    $sth->bindParam("nome", $input['usuario_nome']);
    $sth->bindParam("email", $input['usuario_email']);
    $sth->bindParam("senha", $input['usuario_senha']);
    $sth->execute();
    $input['user_id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

// Retrieve todo with id 
$app->get('/usuario/profile/[{id_usuario}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM tb_usuario WHERE usu_id = :id");
    $sth->bindParam("id", $args['id_usuario']);
    $sth->execute();
    $todos = $sth->fetchObject();
    return $this->response->withJson($todos);
});

// Retrieve all friends
$app->get('/usuario/list_friends/[{id_usuario}]', function (Request $request, Response $response, array $args) {
    
    $sth = $this->db->prepare("SELECT * FROM tb_usuario
                                WHERE usu_id IN (SELECT rel_friends_friend
                                                    FROM tb_relationships_friends
                                                    WHERE rel_friends_user = :idUsuario)
                                AND usu_id != :idUsuario
                            ");
    $sth->bindParam("idUsuario", $args['id_usuario']);

    $sth->execute();
    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);
});


// Retrieve all friends recommended
$app->get('/usuario/recommended_friends/[{id_usuario}]', function (Request $request, Response $response, array $args) {

    $sth = $this->db->prepare("SELECT * FROM tb_usuario
                                WHERE usu_id NOT IN (SELECT rel_friends_friend
                                                    FROM tb_relationships_friends
                                                    WHERE rel_friends_user = :idUsuario)
                                AND usu_id != :idUsuario
                            ");

    $sth->bindParam("idUsuario", $args['id_usuario']);

    $sth->execute();
    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);
});

// Retrieve all usuarios
$app->get('/usuario/listall', function (Request $request, Response $response, array $args) {
    $sth = $this->db->prepare("SELECT * FROM tb_usuario ORDER BY usu_id");
    $sth->execute();
    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);
});

// Login
$app->get('/usuario/login/[{email},{senha}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM tb_usuario WHERE usu_email = :email AND usu_senha = :senha");
    //$query = "%" . $args['query'] . "%";
    $user = $args['email'];
    $senha = $args['senha'];
    $sth->bindParam("email", $user);
    $sth->bindParam("senha", $senha);
    $sth->execute();
    $todos = $sth->fetchObject();
    return $this->response->withJson($todos);
});

// DELETE a todo with given id
$app->delete('/usuario/delete/[{id_usuario}]', function ($request, $response, $args) {

    $sth = $this->db->prepare("DELETE FROM tb_post WHERE tb_post_id_user = :idPost");
    $sth->bindParam("idPost", $args['id_usuario']);
    $sth->execute();

    $sth = $this->db->prepare("DELETE FROM tb_relationships_friends WHERE rel_friends_user = :idRel");
    $sth->bindParam("idRel", $args['id_usuario']);
    $sth->execute();

    $sth = $this->db->prepare("DELETE FROM tb_relationships_friends WHERE rel_friends_friend = :idRel");
    $sth->bindParam("idRel", $args['id_usuario']);
    $sth->execute();

    $sth = $this->db->prepare("DELETE FROM tb_usuario WHERE usu_id = :idUser");
    $sth->bindParam("idUser", $args['id_usuario']);
    $sth->execute();

    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);
});

// Update todo with given id
$app->put('/usuario/edit/[{id_usuario}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE tb_usuario 
            SET usu_nome = :nome 
            WHERE usu_id = :id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $args['id_usuario']);
    $sth->bindParam("nome", $input['nome_usuario']);
    $sth->execute();
    $input['id_usuario'] = $args['id_usuario'];
    return $this->response->withJson($input);

});

// Retrieve todo with id 
$app->post('/relationship/add', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO tb_relationships_friends (rel_friends_user, rel_friends_friend) VALUES (:idUser, :idFriend)";
    $sth = $this->db->prepare($sql);
    //$sth->bindParam("nome", $input['part_nome']);
    $sth->bindParam("idUser", $input['id_user']);
    $sth->bindParam("idFriend", $input['id_friend']);
    $sth->execute();
    $input['rel_friends_id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

// REDE SOCIAL
// Add a new todo
$app->post('/post/add', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO tb_post (tb_post_msg, tb_post_foto, tb_post_date, tb_post_id_user) VALUES (:msg, :foto, :dateAddPost, :idUSer)";
    $sth = $this->db->prepare($sql);
    //$sth->bindParam("nome", $input['part_nome']);
    $sth->bindParam("msg", $input['tb_post_msg']);
    $sth->bindParam("foto", $input['tb_post_foto']);
    $sth->bindParam("dateAddPost", $input['tb_post_date']);
    $sth->bindParam("idUSer", $input['tb_post_id_user']);
    $sth->execute();
    $input['user_id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

// Retrieve all participantes
$app->get('/posts/listall', function (Request $request, Response $response, array $args) {

    $sth = $this->db->prepare("SELECT * FROM tb_post ORDER BY tb_post_id DESC");
    $sth->execute();
    $todos = $sth->fetchAll();

    return $this->response->withJson($todos);
});

// Retrieve all participantes
$app->get('/posts/list/[{id_usuario}]', function (Request $request, Response $response, array $args) {

    $sth = $this->db->prepare(" SELECT p.*, u.usu_nome, u.usu_email FROM tb_post as p
                                INNER JOIN tb_usuario as u
                                ON p.tb_post_id_user = u.usu_id
                                WHERE tb_post_id_user IN (SELECT rel_friends_friend 
                                                            FROM tb_relationships_friends 
                                                            WHERE rel_friends_user = :idUsuario) 
                                OR tb_post_id_user = :idUsuario
                                ORDER BY tb_post_id DESC ");

    $sth->bindParam("idUsuario", $args['id_usuario']);

    $sth->execute();
    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);

});


// Retrieve all participantes
$app->get('/posts/listAll/[{id_usuario}]', function (Request $request, Response $response, array $args) {

    $sth = $this->db->prepare(" SELECT * FROM tb_post 
                                WHERE tb_post_id_user IN (SELECT rel_friends_friend 
                                                            FROM tb_relationships_friends 
                                                            WHERE rel_friends_user = :idUsuario) 
                                OR tb_post_id_user = :idUsuario
                                ORDER BY tb_post_id DESC ");

    $sth->bindParam("idUsuario", $args['id_usuario']);

    $sth->execute();
    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);

});

// DELETE a todo with given id
$app->delete('/post/delete/[{id_post}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("DELETE FROM tb_post WHERE tb_post_id = :id");
    $sth->bindParam("id", $args['id_post']);
    $sth->execute();
    $todos = $sth->fetchAll();
    return $this->response->withJson($todos);
});